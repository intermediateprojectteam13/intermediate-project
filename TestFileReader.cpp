#include "FileReader.hpp"
#include <string>
#include <cassert>

using std::cout;
using std::endl;

class FileReaderTest{
public:
	static void test_fileReader() {
		// declare test strings
		const char* test1 = "This is a test";
		const char* test2 = "This is a different test";
		const char* test3 = "This is a test";
		// insert 1 string into test 1
		std::ofstream ofptr("test1.bin", std::ios::out|std::ios::binary); 
		ofptr.write(test1, 14);
		ofptr.close();
		// insert 1 string into test 2
		ofptr.open("test2.bin", std::ios::out|std::ios::binary); 
		ofptr.write(test2, 24);
		ofptr.close();
		// insert 1 string into test 3
		ofptr.open("test3.bin", std::ios::out|std::ios::binary); 
		ofptr.write(test3, 14);
		ofptr.close();

		// bunch of asserts
		// basically test 1 == test 3	
		// all there is to it
		assert(BinaryFileReader::fileeq("test1.bin", "test1.bin"));
		assert(BinaryFileReader::fileeq("test2.bin", "test2.bin"));
		assert(!BinaryFileReader::fileeq("test2.bin", "test1.bin"));
		assert(!BinaryFileReader::fileeq("test1.bin", "test2.bin"));
		assert(BinaryFileReader::fileeq("test3.bin", "test3.bin"));
		assert(BinaryFileReader::fileeq("test1.bin", "test3.bin"));
		assert(BinaryFileReader::fileeq("test3.bin", "test1.bin"));
		assert(!BinaryFileReader::fileeq("test2.bin", "test3.bin"));
		assert(!BinaryFileReader::fileeq("test3.bin", "test2.bin"));
		assert(!BinaryFileReader::fileeq("", ""));

	}
};

int main () {
	cout << endl << "Running FileReaderTest..." << endl;
	FileReaderTest::test_fileReader();
	cout << "Passed all FileReaderTests!" << endl << endl;
}
