#include "Human.hpp"
#include <cassert> 
#include <cstdio> 

using std::cout;
using std::endl;

class HumanTest {

public:
	static void constructorTest() {
		Stock stock(35);
		Human testHuman("Nitin", stock);
		assert(testHuman.name == "Nitin");
		assert(testHuman.stock.isEmpty());
		assert(testHuman.turnStatus == false);
		assert(testHuman.hand.isEmpty());
		assert(testHuman.discard[0].isEmpty());
		assert(testHuman.discard[1].isEmpty());
		assert(testHuman.discard[2].isEmpty());
		assert(testHuman.discard[3].isEmpty());
	}

	static void playFromHandTest() {
		// set up with test card and skip bo
		Stock stock(35);
		Human testHuman("Nitin", stock);
		Card testCard1(3);
		Card testCard2(0);
		Card testCard3(3);
		assert(testCard1.getVal() == 3);
		assert(!testCard1.isSkipBo());
		assert(testCard2.getVal() == 0);
		assert(testCard2.isSkipBo());
		testHuman.hand.addCardToHand(testCard1);
		testHuman.hand.addCardToHand(testCard2);
		
		//Test 1: Non Skip-Bo
		if(!freopen("card.txt", "r", stdin)) cout << "File does not exist!" << endl;
		std::vector<int> testBuild = {2, 6, 7, 8};
		Card returnCard = testHuman.playFromHand(testBuild);
		cout << endl;
		assert(!returnCard.isSkipBo());
		assert(returnCard.getVal() == 3);

		//Test 2: Skip-Bo
		returnCard = testHuman.playFromHand(testBuild);
		cout << endl;
		assert(returnCard.isSkipBo());
		assert(returnCard.getVal() == 0);
		assert(testHuman.hand.isEmpty());

		/*try {	
			// Readd cards
			testHuman.hand.addCardToHand(testCard3);
			//Test 3: Exceptions
			// Assess in console
			// No eligible build
			// Input is 3
			std::vector<int> testBuild2 = {4,5,6,7};	// new testBuild Array [4, 6, 7, 8]
			returnCard = testHuman.playFromHand(testBuild2);
			cout << endl;
		} catch (NoEligibleBuildException& e) {
		}*/
	}
	
	static void playFromDiscardTest() {
		// set up with test card and skip bo
		Stock stock(35);
		Human testHuman("Nitin", stock);
		Card testCard1(3);
		Card testCard2(0);
		Card testCard3(4);
		assert(testCard1.getVal() == 3);
		assert(!testCard1.isSkipBo());
		assert(testCard2.getVal() == 0);
		assert(testCard2.isSkipBo());
		testHuman.discard[0].addCard(testCard1);
		testHuman.discard[1].addCard(testCard2);
		testHuman.discard[2].addCard(testCard3);
		
		//Test 1: Non Skip-Bo
		std::vector<int> testBuild = {2,6,7,8};
		Card returnCard = testHuman.playFromDiscard(testBuild);
		cout << endl;
		assert(!returnCard.isSkipBo());
		assert(returnCard.getVal() == 3);

		//Test 2: Skip-Bo
		returnCard = testHuman.playFromDiscard(testBuild);
		cout << endl;
		assert(returnCard.isSkipBo());
		assert(returnCard.getVal() == 0);
		assert(testHuman.hand.isEmpty());

		/*try {	
			//Test 3: Exceptions
			// Assess in console
			// Invalid choice 
			// Input is 5, max is 4
			int testBuild2[] = {4,5,6,7};	// new testBuild Array [4, 6, 7, 8]
			returnCard = testHuman.playFromDiscard(testBuild2);
			cout << endl;
		} catch (InvalidChoiceException& e) {
		}*/		
	} 
	
	static void playFromStockTest() {
		// set up with test card and skip bo
		Stock stock(35);
		Card testCard1(4);
		Card testCard2(0);
		Card testCard3(3);
		assert(testCard1.getVal() == 4);
		assert(!testCard1.isSkipBo());
		assert(testCard2.getVal() == 0);
		assert(testCard2.isSkipBo());
		stock.addCard(testCard1);
		stock.addCard(testCard2);
		stock.addCard(testCard3);
		Human testHuman("Nitin", stock);

		//Test 1: Non Skip-Bo
		std::vector<int> testBuild = {2,6,7,8};
		Card returnCard = testHuman.playFromStock(testBuild);
		cout << endl;
		assert(!returnCard.isSkipBo());
		assert(returnCard.getVal() == 3);

		//Test 2: Skip-Bo
		returnCard = testHuman.playFromStock(testBuild);
		cout << endl;
		assert(returnCard.isSkipBo());
		assert(returnCard.getVal() == 0);
		assert(testHuman.hand.isEmpty());
		
	}

	static void shiftCardTest() {
		// set up with test card and skip bo
		Stock stock(35);
		Human testHuman("Nitin", stock);
		Card testCard1(3);
		Card testCard2(0);
		Card testCard3(3);
		assert(testCard1.getVal() == 3);
		assert(!testCard1.isSkipBo());
		assert(testCard2.getVal() == 0);
		assert(testCard2.isSkipBo());
		testHuman.hand.addCardToHand(testCard1);
		assert(testHuman.hand.getTopCard().getVal() == 3);
		testHuman.hand.addCardToHand(testCard2);
		assert(testHuman.hand.getNumCards() == 2);
		assert(testHuman.discard[1].isEmpty());
		
		//Test 1: success case
		testHuman.shiftCard();
		assert(testHuman.discard[1].getTopCard().getVal() == 3);
		assert(testHuman.hand.getNumCards() == 1);

	}

	static void cardMoveTest() {
		Stock stock(35);
		Card testCard1(3);
		Card testCard2(0);
		assert(testCard1.getVal() == 3);
		assert(!testCard1.isSkipBo());
		assert(testCard2.getVal() == 0);
		assert(testCard2.isSkipBo());
		Human testHuman("Nitin", stock);
		std::vector<int> testBuild = {2,6,7,8};
		
		//Test 1: non-skipBo 
		Move* noSkipMove = testHuman.cardMove(testCard1, testBuild);
		assert(noSkipMove->getCard().getVal() == 3);
		assert(noSkipMove->getBuildChoice() == -1);
		assert(noSkipMove->dispState() == 1);
		delete noSkipMove;

		//Test 2: skipBo 
		noSkipMove = testHuman.cardMove(testCard2, testBuild);
		assert(noSkipMove->getCard().getVal() == 7);
		assert(noSkipMove->getBuildChoice() == 1);
		assert(noSkipMove->dispState() == 1);
		delete noSkipMove;
	}
};

int main() {

	cout << endl;
	cout << "Running Human tests..." << endl;
	// setup for tests
	std::ofstream cardToPlay("card.txt");
	cardToPlay << 3 << " "<< 0 << " "// << 3 << " "	// play from hand test
		<< 1 << " "<< 2 << " "// << 5 << " "	// play from discard test
		<< 3 << " " << 2 << " "		// shift card test
		<< 2;		// make move test
	cardToPlay.close();
	//
	HumanTest::constructorTest();
	HumanTest::playFromHandTest();
	HumanTest::playFromDiscardTest();
	HumanTest::playFromStockTest();
	HumanTest::shiftCardTest();
	HumanTest::cardMoveTest();
	fclose(stdin);
	cout << "Done with Human tests" << endl;
	cout << endl;

}
