#VARS

CC = g++
CXXFLAGS = -std=c++11 -pedantic -Wall -Wextra -O -g
GPROF = -pg
CGOV = -fprofile-arcs -ftest-coverage
deps = PileTest CardTest TestFileReader HandTest StockTest PlayerTest HumanTest GameTest ComputerTest
linker = Card.o Pile.o Build.o Draw.o Discard.o Hand.o Player.o Human.o Computer.o Game.o

##########
#functions:

driver: SkipBoMain
	./SkipBoMain

SkipBoMain: $(linker) 

test: $(deps) 
	echo "Running test..."
	./TestFileReader
	./PileTest
	./HandTest
	./StockTest
	./PlayerTest
	./HumanTest
	./GameTest
	./ComputerTest

#recompiles tests with gcov on and reports the lines not tested
testgcov:
	make clean
	make CXXFLAGS="$(CXXFLAGS) $(GCOV)" test
	gcov *.cpp
	-@grep -n "#####" *.cpp.gcov

################
#Test commands
GameTest: Card.o Pile.o Discard.o Hand.o Build.o Draw.o Player.o Computer.o Human.o Game.o 

HumanTest: Card.o Pile.o Discard.o Hand.o Player.o Human.o 

PlayerTest: Card.o Pile.o Discard.o Hand.o Player.o Human.o

PileTest: Card.o Draw.o Build.o Pile.o

ComputerTest: Card.o Pile.o Discard.o Hand.o Player.o Computer.o

CardTest: Card.o

StockTest: Card.o Pile.o

HandTest: Card.o Hand.o Pile.o

TestFileReader:

################
#Linker Files

Pile.o: Card.o

Card.o:

Build.o: Card.o

Draw.o: Card.o

Discard.o: Card.o

Hand.o: Card.o

Player.o: Card.o Pile.o Discard.o Hand.o

Human.o: Player.o

Computer.o: Player.o

Game.o: Player.o Human.o Computer.o Pile.o Build.o Draw.o

clean:
	rm -f a.out *~ *.o *.gcov *.gcda *.gcno gmon.out *.bin *.dat *.txt SkipBoMain $(deps)
