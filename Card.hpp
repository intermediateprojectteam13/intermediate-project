#ifndef SB_CARD_H
#define SB_CARD_H
#define SKIPBO_VAL 0


class Card {
  friend class CardTestClass;
  //private:
  int val; //the face value of the Card
  bool skipBo; //whether or not the Card is a skipBo

 public:
  Card(); //no-arugment constructor
  Card(int v); //1-argument constructor
  Card(const Card&); //copy-constructor
  bool isSkipBo() const; //checks if the Card is a skipBo
  void setVal(int v); //sets the Card value
  int getVal() const; //returns the face value of a Card

};

#endif
