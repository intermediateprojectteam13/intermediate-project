#ifndef PILE_EMPTY_EXCEPTION
#define PILE_EMPTY_EXCEPTION

#include <iostream>
#include <exception>

class PileEmptyException : public std::exception {
public:
  virtual const char* what() const throw() {
    return "PileEmptyException: No cards to remove!\n";
  }
};

#endif
