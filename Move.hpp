#ifndef SB_MOVE_H
#define SB_MOVE_H

#include "Card.hpp"

class Move {
	friend class ComputerTest;
//private:
	Card card;
	bool isCard;
	bool isSave;
	bool seeTable;
	int buildChoice;

public:
	Move(Card c) : card(c), isCard(true), isSave(false), seeTable(false), buildChoice(-1) {}
	Move(Card c, int buildChoice) : card(c), isCard(true), isSave(false), seeTable(false), buildChoice(buildChoice) {}
	Move(bool save, bool table) : isCard(false), isSave(save), seeTable(table) {} //comment
	int dispState() {
		return (seeTable * 4 + isSave * 2 + isCard * 1);
	}
	Card getCard() const {return this->card;}
	int getBuildChoice() const {return this->buildChoice;}

};

#endif
