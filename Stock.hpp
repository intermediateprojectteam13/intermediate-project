#ifndef SB_STOCK_H
#define SB_STOCK_H

#include "Pile.hpp"
#include <vector> 
#include <fstream> 

class Stock : public Pile {
	// Nothing special about its data vector
		
public:
	//Constructors
	//Stock(int num);
	Stock(int num) { this->data.reserve(num); }
	Stock(std::vector<Card> cardDeck) { this->data = cardDeck; }

	// Special functions
	int getCardsLeft() const{ return data.size(); }
	
	// Inherited functions
	/*
	void display();
	
	void addCard(Card toAdd) { this->data.push_back(toAdd); }// Should only call during construction
	
	Card removeCard();
	
	Card getTopCard() { return this->data.back(); }
	
	bool isEmpty() { return this->data.size() == 0; }
	*/
	
};

#endif

