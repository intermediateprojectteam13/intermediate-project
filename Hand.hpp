#ifndef SB_HAND_HPP
#define SB_HAND_HPP

#include "Pile.hpp"
#include "NotInHandException.hpp"
//#include "Card.h"

#include <vector>
#include <fstream>


//using std::vector;

class Hand : public Pile {
  //vector<Card>::iterator i = data.begin();
	
  friend class Computer;
  friend class HandTest;
	
public:
  // Contdstructor(s?)
  Hand();
	
  bool isFull() const {return (data.size() == 5);}
	
  // Redefined Pile functions
  void display() throw(PileEmptyException);
	
  // Redefined with parameter since need to know which one to get rid of
  Card removeCard(int toRemove) throw(PileEmptyException, NotInHandException);
	
  bool addCardToHand(Card toAdd); //adds a Card to Hand

  bool checkCard(int toCheck) throw(PileEmptyException); //checks if the Card is in the Hand
        
};

#endif

