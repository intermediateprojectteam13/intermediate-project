#include "Computer.hpp"

#include <cstdlib>
#include <ctime>
#include <unistd.h>
//#include <cassert>

Computer::Computer(std::string name, Stock& stock) :  Player(name, stock) {
}

Move* Computer::makeMove(std::vector<int> buildVals) {

	// Show all of this for testing purposes
/*
	// Display hand
	std::cout << this->name << "'s hand:" << std::endl;
        if (this->hand.isEmpty()) {
		std::cout << "(empty hand)" << std::endl;
	} else {
		this->hand.display();
	}
	
	// Display stockpile
	std::cout << this->name << "'s stockpile (Cards Left " << this->stock.getCardsLeft() << ")" << std::endl;
        if (this->stock.isEmpty()) {
		std::cout << "(empty stock)" << std::endl;
	} else {
		this->stock.display();
	}
	
	// Display discard piles
	std::cout << this->name << "'s discard piles: "<< std::endl;
	for (int i = 0; i < 4; ++i) {
        	if (this->discard[i].isEmpty()) {
			std::cout << "(empty discard)" << std::endl;
		} else {
			this->displayDiscard();
		}
	}
	
	
*/	
	// End section for testing purposes

	// here is where the Move is made
	sleep(1);
	return decisions(buildVals);
	
}




//Move* Computer::decisions(int (&buildVals)[4]) {
Move* Computer::decisions(std::vector<int> buildVals) {
	
	Card c;
	
	sleep(.5);
/*
	std::cin.clear();
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::cin.get();
*/	
	for (int b = 0; b < 4; ++b) {
		if (!(this->stock.isEmpty())) {
			
			if (this->stock.getTopCard().getVal() == buildVals[b] + 1) {
				c = this->stock.removeCard();
				// THIS IS WHERE RETURN MOVE GOES
				std::cout << this->name << " played a " << buildVals[b]+1 << " from their stockpile to build pile " << b+1 << std::endl;
				return (new Move(c));
				//willEndTurn = false;
			} else if (this->stock.getTopCard().isSkipBo()) {
				c = this->stock.removeCard();
				c.setVal(buildVals[b] + 1);
				std::cout << this->name << " played a Skip-Bo as a " << buildVals[b]+1 << " from their stockpile to build pile " << b+1 << std::endl;

				return (new Move(c, b));
			}
		}
	}

	for (int b = 0; b < 4; ++b) {
		for (std::vector<Card>::iterator hi = this->hand.data.begin(); hi != this->hand.data.end(); ++hi) {
			if ((*hi).getVal() == buildVals[b] + 1) {
				c = this->hand.removeCard((*hi).getVal());
				std::cout << this->name << " played a " << buildVals[b]+1 << " from their hand to build pile " << b+1 << std::endl;

				return (new Move(c));
			
			} else if ((*hi).isSkipBo()) {
				c = this->hand.removeCard((*hi).getVal());
				c.setVal(buildVals[b] + 1);
				std::cout << this->name << " played a Skip-Bo as a " << buildVals[b]+1 << " from their hand to build pile " << b+1 << std::endl;

				return (new Move(c, b));
			}
		}
	

		for (int d = 0; d < 4; ++d) {
			if (!(this->discard[d].isEmpty())) {
				if (this->discard[d].getTopCard().getVal() == buildVals[b] + 1) {
					c = this->discard[d].removeCard();
					std::cout << this->name << " played a " << buildVals[b]+1 << " from their discard pile " << d+1 << " to build pile " << b+1 << std::endl;

					return (new Move(c));
					
				} else if (this->discard[d].getTopCard().isSkipBo()) {
					c = this->discard[d].removeCard();
					c.setVal(buildVals[b] + 1);
					std::cout << this->name << " played a Skip-Bo as a " << buildVals[b]+1 << " from their discard pile " << d+1 << " to build pile " << b+1 << std::endl;

					return (new Move(c, b));
				}
			}
		}
	
	}
	

	// Only gets here if no possible moves
	
	  // So first do equiv of shiftCard()
	srand(time(NULL));
	int randDis = rand() % 4;
	int randCard = ((unsigned)rand()) % this->hand.data.size();
	std::cout << this->name << " played a " << this->hand.data[randCard].getVal() << " from their hand to their discard pile " << randDis+1 << ", ending their turn" << std::endl;
	this->discard[randDis].addCard(this->hand.removeCard(this->hand.data[randCard].getVal()));
	
	std::cout << std::endl << this->name << " has " << this->stock.getCardsLeft() << " cards left in its stockpile." << std::endl << std::endl;
	
	std::cout << "Changing turns... " << std::endl;
	sleep(3);
	  // Now do endTurn()
	this->endTurn();
	
	for (int l = 0; l < SCREEN_CLR; ++l) {
		std::cout << std::endl;
	}

	this->displayTable();
	
	for (int l = 0; l < SCREEN_CLR; ++l) {
		std::cout << std::endl;
	}
	
	return (new Move(false, false));
	
}













