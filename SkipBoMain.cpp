#include "Pile.hpp"
#include "Build.hpp"
#include "Player.hpp"
#include "Human.hpp"
#include "Computer.hpp"
#include "Discard.hpp"
#include "Draw.hpp"
#include "Card.hpp"
#include "Stock.hpp"
#include "Game.hpp"
#include "Move.hpp"
#include "Hand.hpp"
#include "FileNotFoundException.hpp"
#include "InvalidChoiceException.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <exception>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;
using std::exception;

int main() {

	cout << "\t\t\tWelcome to Skip-Bo!" << endl;
	
	bool loadGame;
	while (true) {
		try{
	
			cout << endl << "Would you like to load a saved game? (y/n): ";
			char load;
			cin >> load;
			if (load == 'y') {
				loadGame = true;
				break;
			} else if (load == 'n') {
				loadGame = false;
				break;
			} else {
				throw InvalidChoiceException();
			}
			
		} catch (exception &e) {
			cout << e.what() << endl;
		}
	
	}
	
	if (loadGame) {
		while (true) {
			try {
				cout << "Please enter the name of the dat file without the extension." << 
				endl << "File name: ";
				string fName;
				cin >> fName;
				std::ifstream file(fName + ".dat", std::ios::in|std::ios::binary);
				if (file) file.close();
				else throw FileNotFoundException();
				Game loadedGame(fName);
				int victor = loadedGame.runGame();
				cout << endl;
				cout << "Congratulations " << loadedGame.getPlayerName(victor) << "!" << endl;
				cout << "You Won!" << endl;
			} catch (exception &e) {
				cout << e.what() << endl;
			}
		}
				
		
	} else {
		vector<string> playerNames;
		int numStock;
		while(1) {
			try {	
				cout << "Enter number of players: ";
				int numPlayers = 0;
				cin >> numPlayers;
				if(std::cin.fail() || numPlayers <= 1) {	//in case someone inputs a non int
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					cout << "Too few players" << endl;
					throw InvalidChoiceException();
				}
				cin.clear();
			
				cout << "If computer player, precede player name with \"CPU\"" << endl;
				for (int i = 0; i < numPlayers; i++) {
					string name = "";
					cout << "Enter Player " << i + 1 << "'s name: ";
					while(name=="") getline(cin,name);
					playerNames.push_back(name);
			
				}
				cout << "Enter the initial number of cards in stockpiles: ";
				cin >> numStock;
				if(std::cin.fail() || numStock <= 0) {	//in case someone inputs a non int
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					cout << "Too few players" << endl;
					throw InvalidChoiceException();
				}
				break;
			} catch (exception& e) {
				cout << e.what() << endl;
			}
		}
		
		Game newGame(playerNames, numStock);
		int victor = newGame.runGame();
		cout << endl;
		cout << "Congratulations " << playerNames.at(victor) << "!" << endl;
		cout << "You Won!" << endl;
		
		return 0;
	}
}
