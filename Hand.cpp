//#include "Pile.hpp"
//#include "Card.h"
#include "Hand.hpp"

//Constructor	
Hand::Hand() {
  this->data.clear();
  this->data.reserve(5);
}	
        	
//Overridden Pile functions
void Hand::display() throw(PileEmptyException){
  if (this->data.empty()) {
    throw PileEmptyException();
  }
  int numCard = this->data.size();
  for (int i = 0; i < numCard; i++) {
    std::cout << "*******   ";
  }		
  std::cout << std::endl;
  for (int i = 0; i < numCard; i++) {
    std::cout << "*     *   ";
  }
  std::cout << std::endl;
  for (int i = 0; i < numCard; i++) {
    std::cout << "*";
    if (data[i].isSkipBo()) std::cout << std::setw(5) << "Skip" << "*   ";
    else {
	    std::setfill(' ');
	    std::cout << std::setfill(' ') << std::setw(4) << this->data[i].getVal();
	    std::cout << " *   ";
    }
  }
  std::cout << std::endl;
  for (int i = 0; i < numCard; i++) {
    if (data[i].isSkipBo()) std::cout << "*" <<  std::setw(5) << "-Bo" << "*   ";
    else std::cout << "*     *   ";
  }
  std::cout << std::endl;
  for (int i = 0; i < numCard; i++) {
    std::cout << "*******   ";
  }		
  std::cout << std::endl; 
}
	
bool Hand::addCardToHand(Card toAdd) {
  //can't add a card if the Hand is full
  if (isFull()) {
    return false;
  } else {
    this->data.push_back(toAdd);
    return true;
  }
}
	
// Makes sense to not need to construct a card to use this, just look for an int (WHAT ABOUT SKIP BOS)
// bool removeCardFromHand(Card toRemove) {
Card Hand::removeCard(int toRemove) throw(PileEmptyException, NotInHandException) {		
  if(this->isEmpty()) throw PileEmptyException();
  for (std::vector<Card>::iterator i = this->data.begin(); i != data.end(); ++i) {
    if (i->getVal() == toRemove) {
      Card c = *i;
      data.erase(i);
      return c;
    }
  }		
  throw NotInHandException();
}

/**
 * Goes through the Hand and check if the Card is in the Hand
 */
bool Hand::checkCard(int toCheck) throw(PileEmptyException) {	//check if card exists in hand
  if(this->isEmpty()) throw PileEmptyException();
  for (std::vector<Card>::iterator i = this->data.begin(); i != data.end(); ++i) {
    if (i->getVal() == toCheck) {
      return true;
    }
  }		
  return false;
}
