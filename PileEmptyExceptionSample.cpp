#include <iostream>
#include "PileEmptyException.hpp"

int main() {

  try {
    throw PileEmptyException();
  } catch(PileEmptyException& e) {
    std::cout << e.what() << std::endl;
  }

  return 0;

}

