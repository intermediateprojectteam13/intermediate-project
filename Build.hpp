#ifndef SB_BUILD_H
#define SB_BUILD_H

#include "Pile.hpp"
#include <fstream>

class Build : public Pile {
  friend class Draw; //Draw needs access to Build's Cards
  friend class PileTest; //PileTest test Build functionality
 private:
  bool onTable; //true if the deck has been filled and
                //is ready to be added to draw deck next time

 public:
  Build();
  bool isFull(); //whether or not the Build has reached 12 cards
  void setGarbage(bool); //sets the garbage status of this Build.
                         //Argument will usually be true.
                         //False when doing an undo move.
};

#endif
