#include <fstream>
#include <string>
#include <iostream>

class BinaryFileReader {
	friend class PileTest;
	friend class FileReaderTest;
	friend class PlayerTest;
	static bool fileeq(std::string lhsName, std::string rhsName) {
		const char* lhsChar = lhsName.c_str();
		const char* rhsChar = rhsName.c_str();
		std::ifstream lhs(lhsChar, std::ios::in|std::ios::binary); 
		std::ifstream rhs(rhsChar, std::ios::in|std::ios::binary); 
		//check if files exist
		if (!lhs || !rhs) return false;

		bool match = true;
		//read until both of the files are done or there is a mismatch
		while(!(lhs.eof()) || !(rhs.eof())) {
			if(lhs.eof() ||		// lhs done first
			   rhs.eof() ||		// rhs done first
			   (lhs.get() != rhs.get())) {// chars don't match
			   match = false;
			   break;
			}
		}
		lhs.close();
		rhs.close();
		return match;
	}
};
