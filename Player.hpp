#ifndef SB_PLAYER_H
#define SB_PLAYER_H

#include "Pile.hpp"
#include "Move.hpp"
#include "Discard.hpp"
#include "Stock.hpp"
#include "Hand.hpp"
#include "Draw.hpp"
#include <string>
#include <vector>

#define SCREEN_CLR 150

class Player {

	friend class PlayerTest;
	friend class GameTestClass;
protected:
	std::string name;	// player name
	Stock stock;		// player's stockpile
	Discard discard[4];	// 4 discard piles
	Hand hand;		// player's hand
	bool turnStatus;	// whether it's the players turn
	void endTurn() {this->turnStatus = false;}	// set turnStatus to false

public:
	Player(std::string name, Stock stock);		// default
	virtual ~Player() {}
	virtual Move* makeMove(std::vector<int>) = 0;	// make a Move, return a Move object to be recorded for undo/redo purpose
	bool checkStock() const{return this->stock.isEmpty();}	// return true if stock is empty, false if not
	void startTurn(); 				// set turnStatus to true
	bool isTurn() const{return this->turnStatus;}	// set turnStatus to false
	void displayDiscard() const;			// display discard piles
	void displayTable() const;			// helper function for game's seeTable()
	void convertToBinary(std::ofstream& saveFile);	// convert to binary- save file helper function
	void convertFromBinary(std::ifstream& loadFile);	// convert from binary- load file helper function
	int getCardsLeft() const;			// get how much of stock pile is left
	std::string getName() const;			// get name of player
	Card getTopOfStock() const;			// get the card at the top of stock
	virtual bool isHuman() const = 0;	// return if human or computer 
	bool isHandEmpty() const{return this->hand.isEmpty();} 
	bool isHandFull() const{return this->hand.isFull();} 
	void refillHand(Draw&);		// if hand is empty, game can use this to refill the hand with the draw deck

};

#endif
