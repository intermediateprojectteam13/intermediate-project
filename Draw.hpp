#ifndef SB_DRAW_H
#define SB_DRAW_H

#include "Pile.hpp"
#include "Build.hpp"
#include <fstream> 

class Draw : public Pile {
  friend class PileTest;
 public:
  Draw(); //sets up an initial deck of 162 cards
  void addDeck(); // adds an extra pack of 162 cards in case they run out
  void shuffleDraw(); //shuffles the initial deck
  void shuffle(std::vector<Build>); //shuffles the Draw deck
  bool isEmpty(); //checks if Draw is empty.
};

#endif
