#include "Hand.hpp"

#include <cassert>

using std::cout;
using std::endl;

class HandTest {
public:
	static void multiTest() {
		Hand test;			// Test constructor
		
		assert(test.isEmpty());		// Test isEmpty()
		
		Card c1(1);
		test.addCardToHand(c1);		// Test addCardToHand()
	//	test.addCardToHand(1);
		assert(!(test.isFull()));	// Test isFull()
		assert(!(test.isEmpty()));	
						// More tests...
		Card c2(12);
		test.addCardToHand(c2);
	//	test.addCardToHand(13);
		assert(!(test.isFull()));

		
		Card c3(12);
		test.addCardToHand(c3);
	//	test.addCardToHand(13);
		assert(!(test.isFull()));
		
		Card c4(5);
		test.addCardToHand(c4);
	//	test.addCardToHand(5);
		assert(!(test.isFull()));
		
		Card c5(SKIPBO_VAL);
		test.addCardToHand(c5);
	//	test.addCardToHand(SKIPBO_VAL);
		assert(test.isFull());

		test.display();
		
		assert(test.removeCard(1).getVal() == 1);
		assert(test.removeCard(5).getVal() == 5);
		assert(!(test.isFull()));
		//assert(test.removeCard(1).getVal() == -1);
		assert(!(test.isFull()));
		assert(test.removeCard(SKIPBO_VAL).getVal() == 0);
		assert(!(test.isFull()));
		
	//	Card c6(6);
		//assert(test.removeCard(6).getVal() == -1);	
		
	}
};


int main() {
	cout << endl << "Running Hand tests..." << endl;
	
	//cout << "Testing addCardToHand(), removeCardFromHand(), isFull()... ";
	HandTest::multiTest();
	//cout << "tests passed." << endl;
	
	//cout << "All Hand tests passed. :)" << endl << endl;
	cout << "Passed all Hand tests!" << endl << endl;
	
	
}
