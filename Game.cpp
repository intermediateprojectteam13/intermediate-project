#include "Game.hpp"
#include "Human.hpp"
#include "Computer.hpp"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <unistd.h>

#define BUIL_DISP_LEN 14

Game::Game(std::vector<std::string> playerList, int stockPileSize) : numPlayers(playerList.size()){
	// give everything starting values
	for (int i = 0; i < BUILD_PILES; i++) buildPiles[i] = new Build();	// make build piles
	garbagePile.clear();		// empty garbage pile vector
	players.reserve(this->numPlayers);	// players vector initialization
	// deal cards to stock piles
	int drawPileNum = ((this->numPlayers) * (stockPileSize + 5)) / 162;	// add as many draw decks
	for(int i = 0; i < drawPileNum; i++) this->drawPile.addDeck();		// to fit given conditions
	this->drawPile.shuffleDraw();	// shuffle deck
	for(int i = 0; i < this->numPlayers; i++) {
		Stock stock(stockPileSize);
		for(int i = 0; i < stockPileSize; i++) {
			stock.addCard(this->drawPile.removeCard());
		}
		if (playerList[i][0] == 'C'
		 && playerList[i][1] == 'P'
		 && playerList[i][2] == 'U') players[i] = new Computer(playerList[i], stock);
		else players[i] = new Human(playerList[i], stock);
	}
	// all done!
}

Game::~Game() {
	// Build pile pointers, both build and garbage
	for (int i = 0; i < BUILD_PILES; i++) delete buildPiles[i];	// make build piles
	for (unsigned int i = 0; i < garbagePile.size(); i++) delete garbagePile[i];	// make build piles
	// Get rid of player pointers
	for(int i = 0; i < this->numPlayers; i++) delete players[i];
	
}

int Game::runGame() {
	// actual run game
	while(hasVictory() == -1) {			// check if any of the players have won yet
		for (int i = 0; i < numPlayers; i++) {	//if not iterate through players
			if(!players[i]->isHandFull()) players[i]->refillHand(this->drawPile);	// check if player's hand is empty and needs refilling
		
		
///////////////////////////////////////////
			
	this->seeTable();			
	sleep(5);		
			
///////////////////////////////////////////
		
		
			players[i]->startTurn();	// start player's turn

			while(players[i]->isTurn()) {	// run till turn's over
				std::cout << std::endl << std::endl;
				std::cout << "-----------------------------------------------------------------" << std::endl;
				std::cout << std::endl << std::endl;
				this->checkDraw();	// check if draw pile is empty or no
				if(players[i]->isHandEmpty()) players[i]->refillHand(this->drawPile);	// check if player's hand is empty and needs refilling
				this->displayBuild();	// display build piles
				std::vector<int> buildVals;	// make vector to pass in holding build vals
				buildVals.clear();	// clear all data to be safe 
				buildVals.reserve(4);	// make only space you need
				for (int i = 0; i < BUILD_PILES; i++) {	// setup builVals based on whether pile is empty or not
					if(buildPiles[i]->isEmpty()) buildVals.push_back(0);
					else buildVals.push_back(buildPiles[i]->getTopCard().getVal());
				}
				Move* retMovePtr = players[i]->makeMove(buildVals);
				switch(retMovePtr->dispState()) {	// evaluate what the game should do
									// about the move
					case 1:	//has card, place in build pile
						if (retMovePtr->getBuildChoice() == -1) {
							for (int i = 0; i < BUILD_PILES; i++) {		// code for if regular card
								if(buildVals[i] + 1 == retMovePtr->getCard().getVal()) {	// check which build pile is less than the returned card by 1
									buildPiles[i]->addCard(retMovePtr->getCard());
									break;
								}
							}
						} else buildPiles[retMovePtr->getBuildChoice()]->addCard(retMovePtr->getCard());
						break;
					case 2:	//save game
						{
							std::cout << "Enter save file name: ";
							std::string saveName;
							std::cin >> saveName;
							std::cout << std::endl;
							saveGame(saveName, i);
							break;
						}
					case 4:	//see table
						this->seeTable();
						break;
					default://technically only other case is 0 and nothing should happen
						break;
				}
				delete retMovePtr;
				checkBuild();		// check if last move filled a buildpile
				if (hasVictory() != -1) return hasVictory();	// if player has just won in last turn, break for loop
								// else keep going
			}
		}
	}
	return hasVictory();
}

void Game::seeTable() const{
  std::cout << "-----------------------------------------------------------------" << std::endl;	// give a delimiter border line because won't be printed by default
  this->displayBuild();	// display build piles first
  std::cout << std::endl << std::endl << std::endl;	// spacing
  for (int i = 0; i < this->numPlayers; i++) {	// iterate through players
    std::cout << players[i]->getName() 		// print player and how many stockpile cards they have left
	      << "'s discard piles and stockpile (Cards Left: " 
	      << players[i]->getCardsLeft() << ")" << std::endl;
    players[i]->displayTable();			// run the display helper function within player that accesses private data Game cannot
    std::cout << std::endl;
  }
}

int Game::hasVictory() const{
	for (int i = 0; i < numPlayers; i++) {		//iteratre through players
		if(players[i]->checkStock()) return i;	//check if any has empty stockpile
	}
	return -1;	//if none were empty, return false
}

void Game::checkBuild() {	//check if build piles are full
	for(int a = 0; a < BUILD_PILES; a++) {	//iterate through all build piles
		if (buildPiles[a]->isFull()) {
			buildPiles[a]->setGarbage(true);	// check if specified piles is full/set
								// pile as garbage
			garbagePile.push_back(buildPiles[a]);	// add pile to garbage object
			buildPiles[a] = new Build();	// build new Build in old one's place
		}
	}
}

void Game::checkDraw() {	// refill draw pile if necessary
	if (this->drawPile.getNumCards() < 5) {	// check the number of cards left is less than 5, otherwise okay
		if (this->garbagePile.size() > 0) {	// check if there are garbagePiles
			for (unsigned int i = 0; i < this->garbagePile.size(); i++) {	// iterate through all the garbage pile 
				while (!this->garbagePile[i]->isEmpty()) {	// pull cards from pile till it's empty
					garbagePile[i]->getTopCard().setVal(0);
					this->drawPile.addCard(garbagePile[i]->removeCard());
				}
				delete this->garbagePile[i];	// dynamically allocated build pile needs to be deleted
			}
			this->garbagePile.clear();
		} else {			// otherwise, add and shuffle a new deck
			this->drawPile.addDeck();
		}
		drawPile.shuffleDraw();
	}
}

void Game::displayBuild() const{
        std::cout << "Build Pile:" << std::endl;
	// display buildPiles piles
	std::setfill(' ');
	// Titles for all the card decks
	std::cout << "   1" << std::setw(BUIL_DISP_LEN) << "2" << std::setw(BUIL_DISP_LEN) << "3" << std::setw(BUIL_DISP_LEN) << "4" << std::endl;
	// Card top border
	std::cout << "*******";
	for (int i = 0; i < 3; i++) {
		std::cout << std::setw(BUIL_DISP_LEN) << "*******";
	}
	std::cout << std::endl;
	// Card empty space 
	std::cout << "*     *";
	for (int i = 0; i < 3; i++) {
		std::cout << std::setw(BUIL_DISP_LEN) << "*     *";
	}
	std::cout << std::endl;
	// Card content 
	for (int i = 0; i < 4; i++) {
		if(buildPiles[i]->isEmpty()) {
			std::cout << "*     *";
		} else { 
			std::cout << "*  ";
			std::cout << std::setw(2) << buildPiles[i]->getTopCard().getVal();
			std::cout << " *";
		} 
		std::cout<< "       ";
	}
	std::cout <<std::endl;
	for (int i = 0; i < 4; i++) {
		std::cout << "*     *";
		std::cout<< "       ";
	}
	std::cout << std::endl;
	// Card bottom border
	std::cout << "*******";
	for (int i = 0; i < 3; i++) {
		std::cout << std::setw(BUIL_DISP_LEN) << "*******";
	}
	std::cout << std::endl;
	
}

void Game::saveGame(std::string saveName, int playerNum) const{	//print to .dat
	/* Before actually writing the function, will dictate how the save binary will be formatted
	 * Format must make sure that it's actually saving space to be worth the trouble
	 * Data to be saved:
	 *	- numPlayers
	 * 	- Players
	 *		- computer or human 
	 *		- Player names
	 *		- stockpile
	 *	  	- discard
	 *	 		- values in index
	 *		-hand
	 *	 		- size of hand
	 *	 		- values in hand
	 *		-turn status
	 * 	- buildPiles
	 *		- index (char)
	 *		- values in index
	 *	- garbagePile
	 *		- index (char)
	 *		- values in index
	 *	- drawPile 
*******************************************************************************************
	 *	- for all piles:
	 *		- size of pile
	 *		- data values of pile
	 */
	std::ofstream saveFile(saveName + ".dat", std::ios::out|std::ios::binary);	//open .dat

	// numPlayers binary
	saveFile.write(reinterpret_cast<const char*>(&(this->numPlayers)), sizeof(char));	//char because small enough

	// Players binary
	for (int i = 0; i < this->numPlayers; i++) {
		bool humanStatus = players[(i+playerNum)%numPlayers]->isHuman();
		saveFile.write(reinterpret_cast<const char*>(&(humanStatus)), sizeof(bool));		//indices
		players[((i+playerNum)%numPlayers)]->convertToBinary(saveFile);		//values
	}

	// Build piles 
	for (int i = 0; i < 4; i++) {
		buildPiles[i]->convertToBinary(saveFile);	//values
	}

	// Garbage piles 
	int garbageSize = this->garbagePile.size();
	saveFile.write(reinterpret_cast<const char*>(&(garbageSize)), sizeof(char));		//indices
	for (unsigned int i = 0; i < this->garbagePile.size(); i++) {
		garbagePile[i]->convertToBinary(saveFile);	//values
	}

	// Draw pile
	drawPile.convertToBinary(saveFile);

	saveFile.close();
	
}

Game::Game(std::string loadName) : drawPile() {	//load from .dat
	/* Before actually writing the function, will dictate how the save binary will be formatted
	 * Format must make sure that it's actually saving space to be worth the trouble
	 * Data to be saved:
	 *	- numPlayers
	 * 	- Players
	 *		- index
	 *		- Player names
	 *		- stockpile
	 *	  	- dis
	 *	 		- values in index
	 *		-hand
	 *		-turn status
	 * 	- buildPiles
	 *		- index
	 *		- values in index
	 *	- garbagePile
	 *		- index
	 *		- values in index
	 */
	std::ifstream loadFile (loadName + ".dat", std::ios::in|std::ios::binary);	//open .dat

	this->numPlayers = 0;
	// numPlayers binary
	loadFile.read(reinterpret_cast<char*>(&(this->numPlayers)), sizeof(char));	//char because small enough
	players.resize(this->numPlayers);

	// Players binary
	for (int i = 0; i < numPlayers; i++) {
		bool isHuman = true;
		loadFile.read(reinterpret_cast<char*>(&isHuman), sizeof(bool));		// check if human or coomputer
		Stock stock(0);								// make BS stock pile
		if(isHuman) players[i] = new Human("", stock);				// make either Human or Computer based on var
		else players[i] = new Computer("", stock);
		players[i]->convertFromBinary(loadFile);		//values
	}

	// Build piles 
	for (int i = 0; i < 4; i++) {
		buildPiles[i] = new Build();			// need to initialize pointer
		buildPiles[i]->convertFromBinary(loadFile);	// values
	}

	// Garbage piles 
	int garbageSize = 0;
	loadFile.read(reinterpret_cast<char*>(&(garbageSize)), sizeof(char));		// number of piles
	for (int i = 0; i < garbageSize; i++) {
		garbagePile.push_back(new Build());
		garbagePile[i]->convertFromBinary(loadFile);	//values
	}

	drawPile.convertFromBinary(loadFile);		//convert draw

	loadFile.close();
	
}
