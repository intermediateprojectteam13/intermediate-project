#include "Stock.hpp"

#include <cassert>

using std::cout;
using std::endl;

// James, you compiled this like this:
//
// g++ -std=c++11 -pedantic -Wall -Wextra -o StockTest -O Card.o Stock.hpp StockTest.cpp
//
//

void getCardsLeftTest() {
	Stock s1(35);
	//cout << SKIPBO_VAL << endl;
	
	for (int i = 0; i < 35; ++i) {
		Card c(i);
		s1.addCard(c);
	}
	
	//cout << s1.getCardsLeft() << endl;
	
	assert(s1.getCardsLeft() == 35);
	s1.removeCard();
	assert(s1.getCardsLeft() == 34);
	s1.removeCard();
	assert(s1.getCardsLeft() == 33);
	
	Stock s2(1);
	Card c1(1);
	s2.addCard(c1);
	Card c2(2);
	s2.addCard(c2);
	
	assert(s2.getCardsLeft() == 2);
	s2.removeCard();
	assert(s2.getCardsLeft() == 1);
	s2.removeCard();
	assert(s2.getCardsLeft() == 0);
	
}




int main() {
	
	cout << endl << "Running Stock tests..." << endl;
	getCardsLeftTest();
	cout << "Passed all Stock tests!" << endl << endl;
	
	
	
}


