#include "Game.hpp"
#include <cassert> 

using std::cout;
using std::endl;

class GameTestClass {

public:
	static void constructorTest() {
		// constructor being tested is not save game constructor
		// that will be another test function
		std::vector<std::string> playerNames = {"Player 1", "Player 2", "Player 3", "CPU Player 1"};
		int stockPileSize = 35;
		
		Game testGame(playerNames, stockPileSize);
		// show num of players is correct
		assert(testGame.numPlayers == 4);
		// show num of cards left in drawPilepile
		assert(testGame.drawPile.getNumCards() == 22);
		// show each player exists
		assert(testGame.players[0]->getName() == "Player 1");
		assert(testGame.players[0]->isHuman());
		assert(testGame.players[1]->getName() == "Player 2");
		assert(testGame.players[1]->isHuman());
		assert(testGame.players[2]->getName() == "Player 3");
		assert(testGame.players[2]->isHuman());
		assert(testGame.players[3]->getName() == "CPU Player 1");
		assert(!testGame.players[3]->isHuman());
		for (int i = 0; i < 4; i ++) {
			assert(testGame.players[i]->getCardsLeft() == 35);
			assert(testGame.buildPiles[i]->isEmpty());
		}
		assert(testGame.garbagePile.size() == 0);
	}

	static void saveLoadTest() {
		// first use normal constructor
		// then save game
		// then load and see if results are same
		std::vector<std::string> playerNames = {"Player 1", "Player 2", "Player 3", "CPU Player 1"};
		int stockPileSize = 35;
		
		Game expectedGame(playerNames, stockPileSize);

		Card c1(1);
		Card c2(2);
		Card c3(3);
		Card c4(4);
		Card c5(5);
		Card c12(12);
		Card c0(0);
		c0.setVal(4);
		expectedGame.buildPiles[0]->addCard(c1);
		expectedGame.buildPiles[1]->addCard(c2);
		expectedGame.buildPiles[2]->addCard(c3);
		expectedGame.buildPiles[3]->addCard(c0);

		// player 1
		expectedGame.players[0]->discard[0].addCard(c1);
		expectedGame.players[0]->discard[1].addCard(c3);
		expectedGame.players[0]->discard[3].addCard(c12);
		
		// players 2
		expectedGame.players[1]->discard[0].addCard(c5);
		expectedGame.players[1]->discard[2].addCard(c4);
		expectedGame.players[1]->discard[3].addCard(c2);
		
		// players 3
		expectedGame.players[2]->discard[0].addCard(c0);
		expectedGame.players[2]->discard[1].addCard(c5);
		expectedGame.players[2]->discard[2].addCard(c12);
		
		// players 4
		expectedGame.players[3]->discard[0].addCard(c2);
		expectedGame.players[3]->discard[1].addCard(c1);
		expectedGame.players[3]->discard[2].addCard(c4);
		expectedGame.players[3]->discard[3].addCard(c2);

		// garbage piles
		expectedGame.garbagePile.push_back(new Build());
		for(int i = 0; i < 10; i++) expectedGame.garbagePile[0]->addCard(c1);

		expectedGame.saveGame("actualSave", 1);


		Game actualGame("actualSave");
		// show num of players is correct
		assert(actualGame.numPlayers == 4);
		// show num of cards left in drawPilepile
		cout << actualGame.drawPile.getNumCards() << endl;
		assert(actualGame.drawPile.getNumCards() == expectedGame.drawPile.getNumCards());
		//assert(actualGame.drawPile.getNumCards() == 22);
		// show each player exists
		assert(actualGame.players[3]->getName() == "Player 1");
		assert(actualGame.players[3]->isHuman());
		assert(actualGame.players[0]->getName() == "Player 2");
		assert(actualGame.players[0]->isHuman());
		assert(actualGame.players[1]->getName() == "Player 3");
		assert(actualGame.players[1]->isHuman());
		assert(actualGame.players[2]->getName() == "CPU Player 1");
		assert(!actualGame.players[2]->isHuman());
		for (int i = 0; i < 4; i ++) {
			//assert(actualGame.players[i]->getCardsLeft() == 35);
			cout << actualGame.players[i]->getCardsLeft() << endl;
			assert(expectedGame.players[i]->getTopOfStock().getVal() == expectedGame.players[i]->getTopOfStock().getVal());
		}
		assert(actualGame.buildPiles[0]->getTopCard().getVal() == 1);
		assert(!actualGame.buildPiles[0]->getTopCard().isSkipBo());
		assert(actualGame.buildPiles[1]->getTopCard().getVal() == 2);
		assert(!actualGame.buildPiles[1]->getTopCard().isSkipBo());
		assert(actualGame.buildPiles[2]->getTopCard().getVal() == 3);
		assert(!actualGame.buildPiles[2]->getTopCard().isSkipBo());
		assert(actualGame.buildPiles[3]->getTopCard().getVal() == 4);
		assert(actualGame.buildPiles[3]->getTopCard().isSkipBo());
		assert(actualGame.garbagePile.size() == 1);
	}

	static void displayTest() {
		// test display build and see table
		Game testGame("actualSave");
		
		//display build
		cout << "Display Build" << endl << endl;
		testGame.displayBuild();
		cout << endl;

		//see table
		cout << "See Table" << endl << endl;
		testGame.seeTable();
		cout << endl;
		
	} 

	static void checkBuildTest() {	
		Game testGame("actualSave");
		Card c1(1);
		Card c2(2);
		Card c3(3);
		Card c4(4);
		Card c5(5);
		Card c12(12);
		Card c0(0);
		c0.setVal(4);

		for(int i = 0; i < 11; i++) testGame.buildPiles[0]->addCard(c1);
		assert(testGame.buildPiles[0]->isFull());
		testGame.checkBuild();
		assert(testGame.buildPiles[0]->isEmpty());
		assert(!testGame.buildPiles[1]->isEmpty());
		assert(!testGame.buildPiles[2]->isEmpty());
		assert(!testGame.buildPiles[3]->isEmpty());
		assert(testGame.garbagePile[1]->isFull());
		assert(testGame.garbagePile.size() == 2);
		testGame.saveGame("actualSave", 0);

		
		
	}

	static void checkDrawTest() {
		Game testGame("actualSave");
		Card c1(1);
		Card c2(2);
		Card c3(3);
		Card c4(4);
		Card c5(5);
		Card c12(12);
		Card c0(0);
		c0.setVal(4);

		// first run with no fixing of draw needing to be done
		int drawSize = testGame.drawPile.getNumCards();
		testGame.checkDraw();
		assert(testGame.drawPile.getNumCards() == drawSize);
		assert(testGame.garbagePile.size() == 2);

		while(!testGame.drawPile.isEmpty()) {
			testGame.drawPile.removeCard();
		}
		
		// first run with garbage pile present
		testGame.checkDraw();
		cout << testGame.drawPile.getNumCards() << endl;
		assert(testGame.drawPile.getNumCards() == 22);
		assert(testGame.drawPile.getTopCard().getVal() == 1);
		assert(testGame.garbagePile.size() == 0);

		while(!testGame.drawPile.isEmpty()) {
			testGame.drawPile.removeCard();
		}

		// first run with garbage pile present
		assert(testGame.garbagePile.size() == 0);
		testGame.checkDraw();
		assert(testGame.garbagePile.size() == 0);
		assert(testGame.drawPile.getNumCards() == 162);
		
		
	}

};

int main() {

	cout << endl;
	cout << "Running Game tests..." << endl;
	GameTestClass::constructorTest();
	GameTestClass::saveLoadTest();
	GameTestClass::displayTest();
	GameTestClass::checkBuildTest();
	GameTestClass::checkDrawTest();
	cout << "Done with Game tests" << endl;
	cout << endl;
	return 0;

}
