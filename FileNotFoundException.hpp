#ifndef SB_FNF_EX
#define SB_FNF_EX
#include <iostream>
#include <exception>

class FileNotFoundException : public std::exception {
public:
  virtual const char* what() const throw() {
    return "FileNotFoundException: File not found in directory!\n";
  }
};

#endif

