#ifndef SB_HUMAN_H
#define SB_HUMAN_H

#include "Player.hpp"
#include "NoEligibleBuildException.hpp"
#include "NotInHandException.hpp"
#include "InvalidChoiceException.hpp"
#include "PileEmptyException.hpp"

class Human : public Player {
	friend class HumanTest;

private:
  Card playFromHand(std::vector<int>&) throw(PileEmptyException, NotInHandException, NoEligibleBuildException, InvalidChoiceException);
  Card playFromDiscard(std::vector<int>&) throw(PileEmptyException, NoEligibleBuildException, InvalidChoiceException);
  Card playFromStock(std::vector<int>&) throw(NoEligibleBuildException, InvalidChoiceException);
  Move* cardMove(Card, std::vector<int>&) const throw(InvalidChoiceException);
  void shiftCard() throw(NotInHandException, InvalidChoiceException);
  
public:
	Human(std::string, Stock);
	Move* makeMove(std::vector<int>);
	void seeTable();
	bool isHuman () const{return true;}

};


#endif
