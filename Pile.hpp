#ifndef SB_PILE_H
#define SB_PILE_H

#include "Card.hpp"
#include "NotInHandException.hpp"
#include "PileEmptyException.hpp"
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iomanip>

class Pile {
  friend class PileTest;
protected:
  std::vector<Card> data; //vector of Cards

public:

  /**
   * Calling the constructor will not do much.
   */
  Pile();

  /**
   * A virtual destructor. This is here so that when we use pointers
   * to refer to Pile and its derived objects, the compiler will
   * know which destructor to call on each object. 
   * For more explanation, look at page 242 in the textbook.
   */
  virtual ~Pile();

  /**
   * Only shows the top Card (last one in vector)
   */
  virtual void display() const throw(PileEmptyException);
  /**
   * Add a Card on top of the Pile (back of the vector)
   */
  virtual void addCard(Card newCard);
  /**
   * Remove the top Card (back of the vector)
   */
  virtual Card removeCard() throw(PileEmptyException, NotInHandException);
  /**
   * Returns the top card in the Pile
   */
  Card getTopCard() const throw(PileEmptyException);
  /**
   * Lets user know if the Pile is empty or not
   */
  bool isEmpty() const;

  /**
   * Returns the number of Cards in the pile.
   */
  int getNumCards() const {return this->data.size();}

  void convertToBinary(std::ofstream & saveFile) const;

  void convertFromBinary(std::ifstream & loadFile);  
};

#endif
