#ifndef SB_NIS_EX
#define SB_NIS_EX
#include <iostream>
#include <exception>

class NotInStockException : public std::exception {
public:
  virtual const char* what() const throw() {
    return "NotInStockException: Card not in stock!\n";
  }
};

#endif

