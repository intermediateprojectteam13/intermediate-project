#include "Human.hpp" 
#include <iostream>
#include <iomanip>
#include <exception>
#define MENU_WIDTH 13

Human::Human(std::string name, Stock stock) : Player(name, stock){
}

Move* Human::makeMove(std::vector<int> buildVals) {
  // Main screen of player
  // Building pile display is handled by Game
  // Display Hand:
  std::cout << this->name << "'s hand:" << std::endl;
  this->hand.display();

  //Display stockpile
  std::cout << this->name << "'s stockPile (Cards Left " << this->stock.getCardsLeft() << ")" << std::endl;
  this->stock.display();
	
  //Display discard piles 
  std::cout << this->name << "'s discard piles: "<< std::endl;
  this->displayDiscard();

  // "Menu screen"
  std::cout << "Select Move: 1 - Play from Hand, 2 - Play from Discard Piles" << std::endl;
  std::cout << std::setw(MENU_WIDTH);
  std::cout << "3 - Play from Stockpile, 4 - See Table" << std::endl;
  std::cout << std::setw(MENU_WIDTH);
  std::cout << "5 - End Turn, 6 - Save Game" << std::endl;
  std::cout << "Selection: ";
  // define choice int to control while loop and be manipulated by menu
	try {
	  int choice = 0;
	  std::cin >> choice;
	  if(std::cin.fail()) {	//in case someone inputs a non int
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	  	throw InvalidChoiceException();
	  }
	  std::cout << std::endl;
	  // use choice to run another function
	  // decided in switch
	  switch(choice) {
		  case 1: 	// play from hand
			return cardMove(this->playFromHand(buildVals), buildVals);
		  case 2: 	// play from discard pile
			return cardMove(this->playFromDiscard(buildVals), buildVals);
		  case 3: 	// play stock pile
			return cardMove(this->playFromStock(buildVals), buildVals);
		  case 4:	//see table
		    return (new Move(false,true));
		  case 5: // end turn
		    this->shiftCard();
		    this->endTurn();
		    return (new Move(false,false));
		  case 6:	// save game
		    return (new Move(true,false));
		  default:
		    // make incorrect choice exception here
			throw InvalidChoiceException(); 
		  }
	} catch (std::exception& e) {
		std::cout << e.what() << std::endl;
	}

	return (new Move(false, false));
			
}

// sub functions for main makeMove

Card Human::playFromHand(std::vector<int>& buildVals) throw(PileEmptyException, NotInHandException, NoEligibleBuildException, InvalidChoiceException) {	// play from hand
  int cardToRemove;							// make var for number card to remove
  std::cout << "Choose card number from hand (enter 0 for Skip-Bo): ";	// prompt for card num
  std::cin >> cardToRemove;						// let user set card num
  std::cout << std::endl;						// endline for cleanliness
  if (cardToRemove > 12 || cardToRemove < 0) throw InvalidChoiceException();	// check if card choice was invalid
  if (cardToRemove == 0) {	// case if card was Skip-Bo
	return this->hand.removeCard(cardToRemove);	// remove skip-bo card, don't worry about anything else
  } else {			// else if not Skip-Bo
	if(!this->hand.checkCard(cardToRemove)) throw NotInHandException();
	for (int i = 0; i < 4; i++) {
		if (buildVals[i] + 1 == cardToRemove) {
			return this->hand.removeCard(cardToRemove);
		}
	}
	throw NoEligibleBuildException();
  }
}

Card Human::playFromDiscard(std::vector<int>& buildVals) throw(PileEmptyException, NoEligibleBuildException, InvalidChoiceException) {

  int disChoice = 0;
  std::cout << "Choose discard pile to play from: ";
  std::cin >> disChoice;
  std::cout << std::endl;
  disChoice--;
  if (disChoice > 3 || disChoice < 0) throw InvalidChoiceException();
  for (int i = 0; i < 4; i++) {
	if (discard[disChoice].getTopCard().getVal() == 0) {
		return this->discard[disChoice].removeCard();
	} else {
		if (buildVals[i] + 1 == discard[disChoice].getTopCard().getVal()) return this->discard[disChoice].removeCard();
	}
  }
  throw NoEligibleBuildException();
}

Card Human::playFromStock(std::vector<int>& buildVals) throw(NoEligibleBuildException, InvalidChoiceException) {
  if(stock.getTopCard().isSkipBo()) return this->stock.removeCard();
  for (int i = 0; i < 4; i++) {
    if (buildVals[i] + 1 == stock.getTopCard().getVal()) {
      return this->stock.removeCard();	 
    }
  }
  throw NoEligibleBuildException();
}

Move* Human::cardMove(Card card, std::vector<int>& buildVals) const throw (InvalidChoiceException){
	if(card.isSkipBo()) {
		std::cout << "Choose build pile to play into: ";
		int buildChoice = 0;
		std::cin >> buildChoice;
		std::cout << std::endl;
		buildChoice--;
		if (buildChoice > 3 || buildChoice < 0) throw InvalidChoiceException();
		card.setVal(buildVals[buildChoice] + 1);
		return (new Move(card, buildChoice));
	} else return (new Move(card));
}

void Human::shiftCard() throw (NotInHandException, InvalidChoiceException){
  int cardToRemove;
  int disChoice;
  std::cout << "Choose card number from hand to discard (0 for Skip-Bo): ";
  std::cin >> cardToRemove;
  if(cardToRemove > 12 || cardToRemove < 0) throw InvalidChoiceException();
  if(!this->hand.checkCard(cardToRemove)) throw NotInHandException();
  std::cout << std::endl << "Choose discard pile to place into: ";
  std::cin >> disChoice;
  disChoice--;
  if(disChoice > 3 || disChoice < 0) throw InvalidChoiceException();
  std::cout << std::endl;
  this->discard[disChoice].addCard(this->hand.removeCard(cardToRemove));
}
