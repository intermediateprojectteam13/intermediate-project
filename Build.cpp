#include "Build.hpp"

/**
 * Constructor for Build
 */
Build::Build() {
  this->data.reserve(12);
}

/**
 * Checks if Build is full. Capacity is 12.
 */
bool Build::isFull() {
  return this->data.size() == 12;
}

/**
 * Sets the Build pile to garbage.
 */
void Build::setGarbage(bool i) {
  this->onTable = i;
}
