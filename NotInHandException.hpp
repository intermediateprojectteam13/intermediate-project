#ifndef SB_NIH_EX
#define SB_NIH_EX
#include <iostream>
#include <exception>

class NotInHandException : public std::exception {
public:
  virtual const char* what() const throw() {
    return "NotInHandException: Card not in hand!\n";
  }
};

#endif
