#ifndef SB_DISCARD_EX
#define SB_DISCARD_EX
#include <iostream>
#include <exception>

class NotInDiscardException : public std::exception {
public:
  virtual const char* what() const throw() {
    return "NotInDiscardException: Card not in discard!\n";
  }
};

#endif

