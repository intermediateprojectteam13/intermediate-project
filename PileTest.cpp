#include "FileReader.hpp"
#include "Pile.hpp"
#include "Draw.hpp"
#include "Build.hpp"
#include <cassert>

using std::cout;
using std::endl;

class PileTest {
public:

  //Test all the basic Pile functions with Draw.
  //Other than that, the individual functions test
  //specific functions in derived classes.

  static void drawPointerTest() {
    std::vector<Pile*> piles;
    piles.push_back(new Draw());
    assert(piles[0]->data.size() == 162);
    assert(piles[0]->data[0].getVal() == 1);
    assert(piles[0]->data[12].getVal() == 2);
    assert(piles[0]->data[24].getVal() == 3);
    assert(piles[0]->data[36].getVal() == 4);
    assert(piles[0]->data[48].getVal() == 5);
    assert(piles[0]->data[161].getVal() == 0);
    delete piles[0];
  }

  static void drawAddCardTest() {
    std::vector<Pile*> piles;
    piles.push_back(new Draw());
    Card c1(13);
    piles[0]->addCard(c1);
    //piles[0]->display();
    assert(piles[0]->data[162].getVal() == 13);
    Card c2(14);
    piles[0]->addCard(c2);
    assert(piles[0]->data[163].getVal() == 14);
    Card c3(15);
    piles[0]->addCard(c3);
    assert(piles[0]->data[164].getVal() != 14);
    delete piles[0];
  }

  static void drawRemoveCardTest() {
    std::vector<Pile*> piles;
    piles.push_back(new Draw());
    Card c1(13);
    piles[0]->addCard(c1);
    Card c2(14);
    piles[0]->addCard(c2);
    Card c3(15);
    piles[0]->addCard(c3);
    assert(piles[0]->data[164].getVal() == 15);
    piles[0]->removeCard();
    assert(piles[0]->data[163].getVal() == 14);
    piles[0]->removeCard();
    assert(piles[0]->data[162].getVal() == 13);
    for (int i = 0; i < 163; i++) {
      piles[0]->removeCard();
    }
    try {
      piles[0]->removeCard();
    } catch (PileEmptyException& pee) {
      cout << pee.what() << endl;
    }
    delete piles[0];
  }

  static void drawGetTopCardTest() {
    std::vector<Pile*> piles;
    piles.push_back(new Draw());
    assert(piles[0]->getTopCard().getVal() == 0);
    piles[0]->removeCard();
    assert(piles[0]->getTopCard().getVal() == 0);
    for (int i = 0; i < 17; i++) {
      piles[0]->removeCard();
    }
    assert(piles[0]->getTopCard().getVal() == 12);
    for (int i = 0; i < 12; i++) {
      piles[0]->removeCard();
    }
    assert(piles[0]->getTopCard().getVal() == 11);
    for (int i = 0; i < 12; i++) {
      piles[0]->removeCard();
    }
    assert(piles[0]->getTopCard().getVal() == 10);
    for (int i = 0; i < 12; i++) {
      piles[0]->removeCard();
    }
    assert(piles[0]->getTopCard().getVal() == 9);
    for (int i = 0; i < 108; i++) {
      piles[0]->removeCard();
    }
    try {
      piles[0]->removeCard();
    } catch (PileEmptyException& pee) {
      cout << pee.what() << endl;
    }
    delete piles[0];
    //piles[0]->display();
  }

  static void drawIsEmptyTest() {
    std::vector<Pile*> piles;
    piles.push_back(new Draw());
    assert(!piles[0]->isEmpty());
    piles[0]->removeCard();
    assert(!piles[0]->isEmpty());
    for (int i = 0; i < 99; i++) {
      piles[0]->removeCard();
    }
    assert(!piles[0]->isEmpty());
    for (int i = 0; i < 62; i++) {
      piles[0]->removeCard();
    }
    assert(piles[0]->isEmpty());
    delete piles[0];
  }

  static void buildPointerTest() {
    std::vector<Pile*> piles;
    piles.push_back(new Build());
    assert(piles[0]->data.capacity() == 12);
    delete piles[0];
  }

  static void buildIsFullTest() {
    try {
      std::vector<Pile*> piles;
      piles.push_back(new Build());
      Build* b1 = dynamic_cast<Build*>(piles[0]);
      assert(piles[0]->data.capacity() == 12);
      Card c1(12);
      piles[0]->addCard(c1);
      assert(!b1->isFull());
      piles[0]->addCard(c1);
      assert(!b1->isFull());
      for (int i = 0; i < 10; i++) {
	piles[0]->addCard(c1);
      }
      assert(b1->isFull());
      delete piles[0];
    } catch (PileEmptyException& e) {
      std::cout << e.what() << std::endl;
    }    
  }

  static void convertToBinaryTest() {
	// expected values output
	std::ofstream expectedFile("expectedBinary.dat", std::ios::out|std::ios::binary);
	// write out size of basic constructed pile
	int sizeOfPile = 162;
	expectedFile.write(reinterpret_cast<const char*>(&sizeOfPile), sizeof(char));
	// print card vals and skipBo stats
	for (int i = 0; i < 162; i++) {
		int boolTrue = 1;
		int boolFalse = 0;
		int skipBoVal = SKIPBO_VAL;
		if (i < 144) {
			expectedFile.write(reinterpret_cast<const char*>(&boolFalse), sizeof(bool));
			int val = (i / 12) + 1;
			expectedFile.write(reinterpret_cast<const char*>(&val), sizeof(char));
		} else {
			expectedFile.write(reinterpret_cast<const char*>(&boolTrue), sizeof(bool));
			expectedFile.write(reinterpret_cast<const char*>(&skipBoVal), sizeof(char));
		}
	}
	expectedFile.close();
	// actual values output
	std::ofstream actualFile("actualBinary.dat", std::ios::out|std::ios::binary);
	Draw samplePile;
	samplePile.convertToBinary(actualFile);
	actualFile.close();
	assert(BinaryFileReader::fileeq("expectedBinary.dat","actualBinary.dat"));
  }

  static void convertFromBinaryTest() {
    // use expected values output from convertTo to create Pile
    std::ifstream expectedFile("actualBinary.dat", std::ios::in|std::ios::binary);
    Draw actualPile;
    actualPile.convertFromBinary(expectedFile);
    expectedFile.close();
    Draw samplePile;
    assert(actualPile.getNumCards() == samplePile.getNumCards());
    while(!actualPile.isEmpty()) { 
      assert(actualPile.removeCard().getVal() == samplePile.removeCard().getVal());
    }
    assert(samplePile.isEmpty());
  }

};


int main() {

  cout << endl << "Running PileTest..." << endl;
  PileTest::drawPointerTest();
  PileTest::drawAddCardTest();
  PileTest::drawRemoveCardTest();
  PileTest::drawGetTopCardTest();
  PileTest::drawIsEmptyTest();
  PileTest::buildPointerTest();
  PileTest::buildIsFullTest();
  PileTest::convertToBinaryTest();
  PileTest::convertFromBinaryTest();
  cout << "Passed all PileTests!" << endl << endl;

  return 0;
}
