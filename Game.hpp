#ifndef SB_GAME_H
#define SB_GAME_H

#include <string>
#include <vector>
#include <array>
#include "Player.hpp"
#include "Pile.hpp"
#include "Draw.hpp"
#include "Build.hpp"

#define BUILD_PILES 4
/* Game class
 * This holds the running data in the game
 * and runs the game. It also reports the final victory and can
 * be written to/read from a binary save file. 
 * Not-templated, so associated game.cpp holds behavior
 */

class Game {
	//private:
		friend class GameTestClass;

		int numPlayers;		//int value for number of players
		std::vector<Player*> players;	// Array of players that is cycled through to let
						// each player run their turn
						// current move
		std::array<Build*,BUILD_PILES> buildPiles;	// Array of build piles in "center of
					// table will always be 4 in array. When one is
					// filled, it will be moved to the garbage
					// vector and a new one will take its place
		Draw drawPile;		// Draw pile_ holds cards to draw from when out
					// of usable cards in hand
		std::vector<Build*> garbagePile;	// vector of filled build piles that will
						// be used to refill the draw pile
		
		friend class Computer;

	public:
		void saveGame(std::string saveName, int playerNum) const;	
						// function that takes current data,
						// changes into binary, and saves
						// into .dat file "saveName.dat"
		Game(std::string loadName);	
						// function that loads from a .dat file
						// loads binary and parses
						// sets up game
						// is essentially another constructor
		Game(std::vector<std::string>, int);	// player array that is cycled through
		~Game();		// delete pointers
		void checkBuild();	// Checks build piles to see if they need
					// to be moved to the garbage pile
					// if they do, moves pile 
		void checkDraw();	// Checks draw piles to see if has enough cards
					// to refill player's hand at some point 
		int runGame();		// Main function that runs game
					// This has the while loop that makes each
					// Player make a move
		int hasVictory() const;	// Check if someone has won 
		void displayBuild() const;	// function to show the build piles above the player screen every turn
		void seeTable() const;	// function that will show all piles any player can see so that players
					// can make strategic decisions
		std::string getPlayerName(int playerNum) {return players[playerNum]->getName();}

};
#endif
