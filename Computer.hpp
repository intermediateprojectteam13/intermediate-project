#ifndef SB_COMPUTER_H
#define SB_COMPUTER_H

#include "Player.hpp"
//#include "NotInDiscardException.hpp"
#include "NotInHandException.hpp"
#include "NotInStockException.hpp"
#include "PileEmptyException.hpp"
#include "Move.hpp"
#include "Stock.hpp"

class Computer : public Player {
//private:
	friend class ComputerTest;
public:
	Move* makeMove(std::vector<int>);
	bool isHuman() const{return false;}
	Computer(std::string, Stock&);
	Move* decisions(std::vector<int>);
	
};

#endif
