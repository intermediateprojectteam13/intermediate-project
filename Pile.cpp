#include "Pile.hpp"

/**
 * Calling the constructor will not do much.
 */
Pile::Pile() {

} //end constructor

  /**
   * A virtual destructor. This is here so that when we use pointers
   * to refer to Pile and its derived objects, the compiler will
   * know which destructor to call on each object.
   * For more explanation, look at page 242 in the textbook.
   */
Pile::~Pile() { }

/**
 * Only shows the top Card (last one in vector)
 */
void Pile::display() const throw(PileEmptyException) {

  //checks if the deck is empty. If emtpy, don't display.
  if (this->data.empty()) {
    throw PileEmptyException();
  }

  //only display the top card of the deck
  Card topCard = data.back();
  //only need to know what value it is
  int cardVal = topCard.getVal();

  //distingiush Skip-Bo card and regular number cards
  if (topCard.isSkipBo()) {
    std::cout << "*******" << std::endl;
    std::cout << "*     *" << std:: endl;
    std::cout << "* Skip*" << std:: endl;
    std::cout << "* -Bo * " << std:: endl;
    std::cout << "*******" << std:: endl;
  } else {
    std::cout << "*******" << std::endl;
    std::cout << "*     *" << std:: endl;
    std::cout << "*  ";
    std::setfill(' ');
    std::cout << std::setfill(' ') << std::setw(2) << cardVal;
    std::cout << " *" << std::endl;
    std::cout << "*     * " << std:: endl;
    std::cout << "*******" << std:: endl;
  }
} //end display()


  /**
   * Add a Card on top of the Pile (back of the vector)
   */
void Pile::addCard(Card newCard) {
  this->data.push_back(newCard); //push to back of vector
} //end addCard()

  /**
   * Remove the top Card (back of the vector)
   */
Card Pile::removeCard() throw(PileEmptyException, NotInHandException) {
  //cannot remove card if the pile is empty
  if (this->data.empty()) {
    throw PileEmptyException();
  }
  //get the top card
  Card toRemove = this->data.back();
  this->data.pop_back();
  return toRemove; //return removed Card
} //end removeCard()


  /**
   * Returns the top card in the Pile
   */
Card Pile::getTopCard() const throw(PileEmptyException) {
  //cannot get the top card if pile is empty
  if (this->data.empty()) {
    throw PileEmptyException();
  }
  return this->data.back();
} //end getTopCard()

  /**
   * Lets user know if the Pile is empty or not
   */
bool Pile::isEmpty() const {
  return this->data.empty();
} //end isEmpty()

void Pile::convertToBinary(std::ofstream & saveFile) const{
  /*
   * First convert size of deck
   * Then convert all data
   */
  typedef std::vector<Card>::size_type vecSize;
  vecSize sizeOfVector = data.size();
  saveFile.write(reinterpret_cast<const char*>(&sizeOfVector), sizeof(char));
  for (vecSize i = 0; i < data.size(); i++) {
    bool skipBoVal = data[i].isSkipBo();
    saveFile.write(reinterpret_cast<const char*>(&skipBoVal), sizeof(bool));
    int cardVal = data[i].getVal();
    saveFile.write(reinterpret_cast<const char*>(&cardVal), sizeof(char));
  }
}

void Pile::convertFromBinary(std::ifstream & loadFile) {
  /*
   * First read size of deck
   * Then convert all data
   */
  // clear any preset data
  this->data.clear();
  // keep going
  typedef std::vector<Card>::size_type vecSize;     // give name to the size_type
  vecSize pileSize = 0;                                     // declare a var of type size_type
    loadFile.read(reinterpret_cast<char*>(&pileSize), sizeof(char));  //read the size of the pile
    this->data.reserve(pileSize);
    for (vecSize i = 0; i < pileSize; i++) {            // iterate based on what known
      // pileSize is
      bool skipBoStatus;                                // make empty bool var skipBo
      int newVal = 0;                                   // make empty int var for val
      loadFile.read(reinterpret_cast<char*>(&skipBoStatus), sizeof(bool));      // read in skipBo status
	if (skipBoStatus) {                               // if skipBo, generate skipBo card
	  Card newCard(SKIPBO_VAL);
	  loadFile.read(reinterpret_cast<char*>(&newVal), sizeof(char));// if skipBo card has val, give val
	    newCard.setVal(newVal);
	  data.push_back(newCard);
	} else {                                          // no skipBo, then just grab val
	  loadFile.read(reinterpret_cast<char*>(&newVal), sizeof(char));
	  Card newCard(newVal);                           // make new card with grabbed
	  data.push_back(newCard);                        // val
	}
    }
}
