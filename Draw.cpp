#include "Draw.hpp"
#include <chrono>
#include <random>
#include <algorithm>

using std::vector;

/**
 * Constructor for Draw will only get 
 * called once during the entire game.
 * When it becomes empty, it'll call the 
 * shuffle function,which will fill up the deck.
 */
Draw::Draw() { 
  data.clear();
  this->addDeck();
}

/**
 * Adds a brand new deck of 144 Cards.
 */
void Draw::addDeck() {

  //Adds 144 numbered Cards
  for (int i = 1; i <= 12; i++) {
    for (int j = 1; j <= 12; j++) {
      Card c(i);
      this->data.push_back(c);
    }
  }

  //Adds 18 Skip-bo Cards
  for (int i = 0; i < 18; i++) {
    Card c(0); //-1 is the 0 defined in Card.h 
    this->data.push_back(c);
  }
  //A total of 162 Cards gets put in order in the data vector

}

/**
 * Called once at the start of the game when the Draw pile initializes 162 Cards.
 */
void Draw::shuffleDraw() {
  //takes in current time as the seed and randomly shuffles the deck
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::shuffle(this->data.begin(), this->data.end(), std::default_random_engine(seed));
}

/**
 * Called when the Draw pile becomes empty.
 */
void Draw::shuffle(vector<Build> builds) {

  //Number of build vectors in the input vector of builds
  int numBuilds = builds.size();

  //Add all the (sorted) Build Cards to the Draw's data
  for (int i = 0; i < numBuilds; i++) {
    for (int j = 0; j < 12; j++) {
      this->data.push_back(builds[i].data[j]);
    }
  }

  //Shuffle the currently sorted Draw
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::shuffle(this->data.begin(), this->data.end(), std::default_random_engine(seed));

}

/**
 * Checks if Draw is empty.
 */
bool Draw::isEmpty() {
  return this->data.size() == 0;
}
