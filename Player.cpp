#include "Player.hpp"
#include <iomanip> 
#include <unistd.h>

#define PLAY_DISP_LEN 14

/**
 * Constructor for Player
 */
Player::Player(std::string name, Stock stock) : name(name), stock(stock), turnStatus(false) {
}

/**
 * Returns the name of the Player.
 */
std::string Player::getName() const {
  return this->name;
}

/**
 * Returns the top Card in the Player's Stock
 */
Card Player::getTopOfStock() const {
  return this->stock.getTopCard();
}

/**
 * Returns how many Cards the Player has left
 * in his/her Stock Pile.
 */
int Player::getCardsLeft() const {
  return this->stock.getCardsLeft();
}

/**
 * Displays the Discard Piles and
 * Stock Pile of the Player.
 */
void Player::displayTable() const {
	// display discard piles
	std::setfill(' ');
	// Titles for all the card decks
	std::cout << "   1" << std::setw(PLAY_DISP_LEN) 
		  << "2" << std::setw(PLAY_DISP_LEN) 
		  << "3" << std::setw(PLAY_DISP_LEN) 
		  << "4" << std::setw(PLAY_DISP_LEN + 4)
		  << "StockPile" << std::endl;
	// Card top border
	std::cout << "*******";
	for (int i = 0; i < 4; i++) {
		std::cout << std::setw(PLAY_DISP_LEN) << "*******";
	}
	std::cout << std::endl;
	// Card empty space 
	std::cout << "*     *";
	for (int i = 0; i < 4; i++) {
		std::cout << std::setw(PLAY_DISP_LEN) << "*     *";
	}
	std::cout << std::endl;
	// Card content 
	for (int i = 0; i < 4; i++) {
		if(discard[i].isEmpty()) {
			std::cout << "*     *";
		} else if (discard[i].getTopCard().isSkipBo()) {
			std::cout << "* Skip*";
		} else { 
			std::cout << "*  ";
			std::cout << std::setw(2) << discard[i].getTopCard().getVal();
			std::cout << " *";
		} 
		std::cout<< "       ";
	}
		if(stock.isEmpty()) {
			std::cout << "*     *";
		} else if (stock.getTopCard().isSkipBo()) {
			std::cout << "* Skip*";
		} else { 
			std::cout << "*  ";
			std::cout << std::setw(2) 
				  << stock.getTopCard().getVal();
			std::cout << " *";
		} 
	std::cout << std::endl;
	for (int i = 0; i < 4; i++) {
		if(discard[i].isEmpty()) {
			std::cout << "*     *";
		} else if (discard[i].getTopCard().isSkipBo()) {
			std::cout << "* -Bo *";
		} else { 
			std::cout << "*     *";
		} 
		std::cout<< "       ";
	}
		if(stock.isEmpty()) {
			std::cout << "*     *";
		} else if (stock.getTopCard().isSkipBo()) {
			std::cout << "* -Bo *";
		} else { 
			std::cout << "*     *";
		} 

	std::cout << std::endl;
	// Card bottom border
	std::cout << "*******";
	for (int i = 0; i < 4; i++) {
		std::cout << std::setw(PLAY_DISP_LEN) << "*******";
	}
	std::cout << std::endl;

}

/**
 * Displays the Discard Pile of the Player.
 */
void Player::displayDiscard() const{
	// display discard piles
	std::setfill(' ');
	// Titles for all the card decks
	std::cout << "   1" << std::setw(PLAY_DISP_LEN) << "2" << std::setw(PLAY_DISP_LEN) << "3" << std::setw(PLAY_DISP_LEN) << "4" << std::endl;
	// Card top border
	std::cout << "*******";
	for (int i = 0; i < 3; i++) {
		std::cout << std::setw(PLAY_DISP_LEN) << "*******";
	}
	std::cout << std::endl;
	// Card empty space 
	std::cout << "*     *";
	for (int i = 0; i < 3; i++) {
		std::cout << std::setw(PLAY_DISP_LEN) << "*     *";
	}
	std::cout << std::endl;
	// Card content 
	for (int i = 0; i < 4; i++) {
		if(discard[i].isEmpty()) {
			std::cout << "*     *";
		} else if (discard[i].getTopCard().isSkipBo()) {
			std::cout << "* Skip*";
		} else { 
			std::cout << "*  ";
			std::cout << std::setw(2) << discard[i].getTopCard().getVal();
			std::cout << " *";
		} 
		std::cout<< "       ";
	}
	std::cout << std::endl;
	for (int i = 0; i < 4; i++) {
		if(discard[i].isEmpty()) {
			std::cout << "*     *";
		} else if (discard[i].getTopCard().isSkipBo()) {
			std::cout << "* -Bo *";
		} else { 
			std::cout << "*     *";
		} 
		std::cout<< "       ";
	}
	std::cout << std::endl;
	// Card bottom border
	std::cout << "*******";
	for (int i = 0; i < 3; i++) {
		std::cout << std::setw(PLAY_DISP_LEN) << "*******";
	}
	std::cout << std::endl;
	
}

/**
 * Starts the turn of the Player.
 * It deals with Hand secrecy by having the previous Player
 * press ENTER and then the next Player also press ENTER
 * afterwards once more.
 * It turns the turn status of the Player to true.
 */
void Player::startTurn() {
	for (int l = 0; l < SCREEN_CLR; ++l) {
		std::cout << std::endl;
	}
	std::cout << "\t\t" << this->name << ", when you are ready to play press ENTER." << std::endl;
	if (this->isHuman()) {
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin.get();

		for (int l = 0; l < SCREEN_CLR; ++l) {
			std::cout << std::endl;
		}
		turnStatus = true;

	} else {
		sleep(1);
		std::cout << std::endl << "\t\t" << this->name << " is deciding what to do..." << std::endl;
		turnStatus = true;
		sleep(2);
	}      

}

void Player::convertToBinary(std::ofstream& saveFile) {

	/*
	 * - Players
	 *	- index (char)
	 *	- Player names
	 *		- name size
	 *		- name data 
	 *	- stockpile
	 *  	- discard
	 * 		- values in index
	 *	-hand
	 * 		- size of hand
	 * 		- values in hand
	 *	-turn status
	 */
	
	// name
	int sizeOfName = name.size();
	const char* usableName = this->name.c_str();
	saveFile.write(reinterpret_cast<const char*>(&sizeOfName), sizeof(char));
	saveFile.write(usableName, sizeof(char) * sizeOfName);
	// stock pile coonversion
	stock.convertToBinary(saveFile);
	// discard piles
	for (int i = 0; i < 4; i++) {
		discard[i].convertToBinary(saveFile);
	}
	// hand
	hand.convertToBinary(saveFile);
	// turn status
	saveFile.write(reinterpret_cast<const char*>(&turnStatus), sizeof(bool));

}

void Player::convertFromBinary(std::ifstream& loadFile) {

	/*
	 * - Players
	 *	- index (char)
	 *	- Player names
	 *		- name size
	 *		- name data 
	 *	- stockpile
	 *  	- discard
	 * 		- values in index
	 *	-hand
	 * 		- size of hand
	 * 		- values in hand
	 *	-turn status
	 */
	
	// name
	int sizeOfName = 0;
	loadFile.read(reinterpret_cast<char*>(&sizeOfName), sizeof(char));
	char* tempName = new char[sizeOfName + 1];
	loadFile.read(tempName, sizeof(char) * sizeOfName);
	tempName[sizeOfName] = '\0';
	this->name = std::string(tempName);
	delete[] tempName;
	// stock
	stock.convertFromBinary(loadFile);
	// discard piles
	discard[0].convertFromBinary(loadFile);
	discard[1].convertFromBinary(loadFile);
	discard[2].convertFromBinary(loadFile);
	discard[3].convertFromBinary(loadFile);
	// hand
	hand.convertFromBinary(loadFile);
	// turn status
	loadFile.read(reinterpret_cast<char*>(&(this->turnStatus)), sizeof(bool));

}

/**
 * Refills the Hand of the Player automatically with the function call.
 */
void Player::refillHand(Draw& drawPile) {

	while(!this->hand.isFull()) {
		this->hand.addCard(drawPile.removeCard());
	}

}
