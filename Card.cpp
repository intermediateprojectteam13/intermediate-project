#include "Card.hpp"

/**
 * Default constructor.
 */
Card::Card() : val(0), skipBo(false) {
} //end constructor1

/**
 * 1-argument constructor.
 */
Card::Card(int v) {
  this->val = v; //sets Card value
  if (v == SKIPBO_VAL) this->skipBo = true;
  else this->skipBo = false;
} //end constructor2

/**
 * Copy constructor.
 */
Card::Card(const Card& other) {
  this->val = other.val;
  this->skipBo = other.skipBo;
}

/**
 * Checks if the Card is a SkipBo Card.
 */
bool Card::isSkipBo() const{
  return skipBo;
} //end isSkipBo()

/**
 * Sets the card value to what the user specifies.
 */
void Card::setVal(int v) {
	if(this->isSkipBo()) this->val = v;
} //end setVal()

/**
 * Returns the value of the Card.
 */
int Card::getVal() const{
  return this->val;
} //end getVal()
