#ifndef SB_NEBE_EX
#define SB_NEBE_EX
#include <iostream>
#include <exception>

class NoEligibleBuildException : public std::exception {
public:
  virtual const char* what() const throw() {
    return "NoEligibleBuildException: Card number choice does not match any build piles!\n";
  }
};

#endif
