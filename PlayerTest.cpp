#include "Player.hpp"
#include "Human.hpp"
#include "FileReader.hpp"
#include <cassert>
#include <iostream>

using std::cout;
using std::endl;

class PlayerTest {
	
	
public:

	static void convertToBinaryTest() {
		/*
		 * - Humans
		 *	- index (char)
		 *	- Human names
		 *		- name size
		 *		- name data 
		 *	- stockpile
		 *  	- discard
		 * 		- values in index
		 *	-hand
		 * 		- size of hand
		 * 		- values in hand
		 *	-turn status
		 */
		// set of cards
		Card c1(1);
		Card c2(2);
		Card c3(3);
		Card c4(4);
		Card c5(5);
		Card c6(6);
		Card c12(12);
		// expected values output
		std::ofstream expectedFile("expectedBinary.dat", std::ios::out|std::ios::binary);
		// write out name + size 
		std::string name = "Divya";
		const char* usableName = name.c_str();
		int sizeOfName = 5; 
		expectedFile.write(reinterpret_cast<const char*>(&sizeOfName), sizeof(char));
		expectedFile.write(usableName, sizeof(char) * sizeOfName);
		// make stock piles
		Stock stock(35);
		for (int i = 0; i < 35; i++) {
			stock.addCard(c1);
		}
		stock.convertToBinary(expectedFile);
		// make discard piles 
		/*
		 * discard[0] = {1}
		 * discard[1] = {2, 12, 4}
		 * discard[2] = {3, 6}
		 * discard[3] = {5}
		 */
		Discard discard[4];
		discard[0].addCard(c1);
		discard[1].addCard(c2);
		discard[1].addCard(c12);
		discard[1].addCard(c4);
		discard[2].addCard(c3);
		discard[2].addCard(c6);
		discard[3].addCard(c5);
		// print discard piles
		// can use convertToBinary in Pile.hpp because tested and proven that it works
		for (int i = 0; i < 4; i++) {
			discard[i].convertToBinary(expectedFile);
		}
		// write hand
		Hand hand;
		hand.convertToBinary(expectedFile);
		// write turnStatus
		bool turnStatus = false;
		expectedFile.write(reinterpret_cast<const char*>(&turnStatus), sizeof(bool));
		
		expectedFile.close();
		// actual values output
		std::ofstream actualFile("actualBinary.dat", std::ios::out|std::ios::binary);
		Human player(name, stock);
		player.discard[0].addCard(c1);
		player.discard[1].addCard(c2);
		player.discard[1].addCard(c12);
		player.discard[1].addCard(c4);
		player.discard[2].addCard(c3);
		player.discard[2].addCard(c6);
		player.discard[3].addCard(c5);

		player.convertToBinary(actualFile);
		actualFile.close();
		
		assert(BinaryFileReader::fileeq("expectedBinary.dat","actualBinary.dat"));
		
	} 

	static void convertFromBinaryTest() {
		// read just generated convertToBinary files
		// use actual just cause, both work
		// name test
		std::ifstream inStream("actualBinary.dat", std::ios::in|std::ios::binary);
		Stock stock(0);
		Human player("Nitin", stock);
		player.convertFromBinary(inStream);
		inStream.close();
		assert(player.name == "Divya");
		// stock test
		int i;
		for(i = 0; !player.stock.isEmpty(); i++) { 
			assert(player.stock.removeCard().getVal() == 1);
			
		}
		// check num of cards in stock
		assert(i == 35);
		// check discard piles
		// discard pile 0
		assert(player.discard[0].removeCard().getVal() == 1);
		assert(player.discard[0].isEmpty());
		// discard pile 1
		assert(player.discard[1].removeCard().getVal() == 4);
		assert(player.discard[1].removeCard().getVal() == 12);
		assert(player.discard[1].removeCard().getVal() == 2);
		// discard pile 2
		assert(player.discard[2].removeCard().getVal() == 6);
		assert(player.discard[2].removeCard().getVal() == 3);
		// discard pile 3
		assert(player.discard[3].removeCard().getVal() == 5);
		assert(!(player.turnStatus));
		
	}

	static void displayDiscardTest() { // qualitative test; look at output for assessment
		Stock stock(35);
		Human testPlayer("test", stock);
		// dis 0 gets empty card
		// dis 1 gets Skip-Bo
		Card dis1(0); 
		// dis 2 gets number 
		Card dis2(3); 
		// dis 3 is skip-bo with val, still should show as skip-bo
		Card dis3(0);
		dis3.setVal(2);
		
		testPlayer.discard[1].addCard(dis1);
		testPlayer.discard[2].addCard(dis2);
		testPlayer.discard[3].addCard(dis3);
		testPlayer.displayDiscard();
	}

	static void displayTableTest() { // qualitative test; look at output for assessment

		Stock stock(35);
		Card c(2);
		stock.addCard(c);
		Human testPlayer("test", stock);
		// dis 0 gets empty card
		// dis 1 gets Skip-Bo
		Card dis1(0); 
		// dis 2 gets number 
		Card dis2(3); 
		// dis 3 is skip-bo with val, still should show as skip-bo
		Card dis3(0);
		dis3.setVal(2);
		
		testPlayer.discard[1].addCard(dis1);
		testPlayer.discard[2].addCard(dis2);
		testPlayer.discard[3].addCard(dis3);
		testPlayer.displayTable();
	}

};

int main() {
	cout << endl << "Running PlayerTest..." << endl;
	PlayerTest::convertToBinaryTest();
	PlayerTest::convertFromBinaryTest();
	PlayerTest::displayDiscardTest();
	PlayerTest::displayTableTest();
	cout << "Passed all PlayerTests!" << endl << endl;
}
