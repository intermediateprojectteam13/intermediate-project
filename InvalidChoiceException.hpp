#ifndef SB_INVALIDC_EX
#define SB_INVALIDC_EX
#include <iostream>
#include <exception>

class InvalidChoiceException : public std::exception {
public:
  virtual const char* what() const throw() {
    return "InvalidChoiceException: Not applicable choice!\n";
  }
};

#endif

