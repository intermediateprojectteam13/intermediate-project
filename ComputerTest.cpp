#include "Computer.hpp"
#include <cassert> 
#include <cstdio> 

using std::cout;
using std::endl;

class ComputerTest {

public:
	static void constructorTest() {
		Stock stock(35);
		Computer testComputer("Thomas", stock);
		assert(testComputer.name == "Thomas");
		assert(testComputer.stock.isEmpty());
		assert(testComputer.turnStatus == false);
		assert(testComputer.hand.isEmpty());
		assert(testComputer.discard[0].isEmpty());
		assert(testComputer.discard[1].isEmpty());
		assert(testComputer.discard[2].isEmpty());
		assert(testComputer.discard[3].isEmpty());
	}


	static void bigTest() {
		
		Stock stock(35);
		Computer testComp("Thomas", stock);
		
		Card s1(1);
		testComp.stock.addCard(s1);
		Card s12(12);
		testComp.stock.addCard(s12);

		
		assert(testComp.stock.getTopCard().getVal() == 12);
		
		std::vector<int> buildVals = {6, 3, 2, 0};
		
		Card h1(1);
		testComp.hand.addCardToHand(h1);
		
		assert(!testComp.hand.isFull());
		assert(!testComp.hand.isEmpty());
		
		Move* m1 = testComp.makeMove(buildVals);	// MOVE ONE
		
//		cout << "\t MOVE ONE COMPLETE" << endl;
		
		assert(testComp.hand.isEmpty());
		assert(m1->card.getVal() == 1);
		
		buildVals = {6, 3, 2, 1};
		
		Card h8(8);
		testComp.hand.addCardToHand(h8);
		Card h9(9);
		testComp.hand.addCardToHand(h9);
		Card h10(10);
		testComp.hand.addCardToHand(h10);
		Card h11(11);
		testComp.hand.addCardToHand(h11);
		Card hSB(0);
		testComp.hand.addCardToHand(hSB);
		
		assert(hSB.isSkipBo());
		
		Move* m2 = testComp.makeMove(buildVals);	// MOVE ONE
		
//		cout << "\t MOVE TWO COMPLETE" << endl;
		
		assert(!testComp.hand.isFull());
		assert(!testComp.hand.isEmpty());
		
		assert(m2->card.getVal() == 7);
		assert(m2->card.isSkipBo());
		
		buildVals = {7, 3, 2, 1};
		
		Move* m3 = testComp.makeMove(buildVals);
		
//		cout << "\t MOVE THREE COMPLETE" << endl;
		
		//assert(testComp.hand.data.size() == 3);
		
		assert(m3->card.getVal() == 8);
		assert(!m3->card.isSkipBo());
		
		buildVals = {8, 3, 2, 1};
		
		Move* m4 = testComp.makeMove(buildVals);
		
//		cout << "\t MOVE FOUR COMPLETE" << endl;
		
		assert(m4->card.getVal() == 9);
		
		
		buildVals = {9, 3, 2, 1};
		
		Move* m5 = testComp.makeMove(buildVals);
		
//		cout << "\t MOVE FIVE COMPLETE" << endl;
		
		assert(m5->card.getVal() == 10);
		
		buildVals = {10, 3, 2, 1};
		
		Move* m6 = testComp.makeMove(buildVals);
		
//		cout << "\t MOVE SIX COMPLETE" << endl;
		
		assert(m6->card.getVal() == 11);
		assert(testComp.hand.isEmpty());
		
		
		assert(testComp.stock.getCardsLeft() == 2);
		
		buildVals = {11, 3, 2, 1};
		
		Move* m7 = testComp.makeMove(buildVals);
		
//		cout << "\t MOVE SEVEN COMPLETE" << endl;
		
		assert(testComp.stock.getCardsLeft() == 1);
		assert(testComp.stock.getTopCard().getVal() == 1);
		
		assert(m7->card.getVal() == 12);
		
		buildVals = {0, 3, 2, 1};
		
		Move* m8 = testComp.makeMove(buildVals);
		
//		cout << "\t MOVE EIGHT COMPLETE" << endl;
		
		assert(m8->card.getVal() == 1);
		
		assert(testComp.hand.isEmpty());
		
		for (int i = 0; i < 5; ++i) {
			testComp.hand.addCardToHand(h1);
		}
		assert(testComp.hand.isFull());
		
		
		buildVals = {1, 3, 2, 1};
		
		Move* m9 = testComp.makeMove(buildVals);
		
//		cout << "\t MOVE NINE COMPLETE" << endl;
		
		assert(!m9->isCard);
		assert(!m9->isSave);
		assert(!testComp.hand.isFull());
		
		bool inDiscard = false;
		for (int i = 0; i < 4; ++i) {
			if (!testComp.discard[i].isEmpty()) {
				assert(testComp.discard[i].getTopCard().getVal() == 1);
				inDiscard = true;
			}
		}
		assert(inDiscard);

		delete m1;
		delete m2;
		delete m3;
		delete m4;
		delete m5;
		delete m6;
		delete m7;
		delete m8;
		delete m9;
		
		
	}




};

int main() {

	cout << endl;
	cout << "Running Computer tests..." << endl;
	ComputerTest::constructorTest();
//	ComputerTest::playFromHandTest();
	ComputerTest::bigTest();
	cout << "Done with Computer tests" << endl;
	cout << endl;

}

